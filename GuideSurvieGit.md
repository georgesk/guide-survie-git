---
title: "Git : un guide de survie"
author: [Georges Khaznadar]
date: "2020-03-21"
keywords: [Git, Example]
lang: fr
options: [colorlinks, toccolor]
...

Git : un guide de survie
========================

Git est un puissant système de gestion de versions. Déjà perdu un document
important ? Avec Git ça n'arrivera plus, et on peut retrouver une copie du
document à chacune des étapes de sa mise au point. Git a été créé par
Linus Torvalds, et ce système permet actuellement à un millier d'auteurs
de mettre à jour le million de lignes du code de Linux.

Premier pas:
------------

Premièrement, on vérifie que le logiciel `git` soit bien installé ; ensuite
on peut commencer le travail.

### L'installation de Git : ###
 * sous GNU-Linux, utilisez le gestionnaire de paquets de votre distribution
 * sous Windows, installez l'application Git-Scm depuis https://git-scm.com/

### Je vérifie que Git est bien installé : ###
 * sous GNU-Linux, on ouvre un terminal et on tape la commande
   ``git --version``.
   Si ça répond poliment quelque chose comme `git version 2.20.1`, c'est
   bon, le système est bien là.
 * sous Windows, si l'installation de Git-Scm s'est bien passée, on doit
   pouvoir trouver sur le bureau, ou dans les menus, la commande
   `Git-bash` : c'est celle-là qu'il faut choisir, pour pouvoir pratiquer
   à l'identique sous Linux comme sous Windows.
   
Sous GNU-Linux, toutes les explications suivantes supposent qu'un terminal est
ouvert et qu'on « a la main » sur ce terminal-là, autrement dit qu'on peut
y lancer des commandes. Sous Windows, les explications suivantes supposent
qu'on a lancé l'application `Git-bash` : cette application ouvre un terminal
très semblable à l'autre.

### Avant de travailler, je décline mon identité. ###
Git tient un journal des modifications faites aux fichiers de travail,
en précisant qui a fait quoi. Donc il faut lui donner une identité
(éventuellement factice). Imaginons que « Marie Curie » commence à utiliser
Git, elle décline son identité en deux commandes :


```console
marie@ordinateur:~$ git config --global user.name "Marie Curie"
marie@ordinateur:~$ git config --global user.email "marie.curie@serveur-de-mail.com"
```

Ces commandes sont à adapter à votre cas particulier.

Mon premier dépôt Git
---------------------

Le guide de survie prend pour exemple la création d'un gestionnaire de
plylists, c'est à dire un logiciel qui permet de gérer des listes de
morceaux de musique enregistrée.

### Je crée un dossier, j'y place un fichier au moins ###

On commence par créer un dossier `playlist` ; pour les noms de dossiers
et de fichiers, il est préférable d'éviter certains caractères, comme
les espaces, et plus généralement tous les caractères qui ont par eux-même
du sens pour le `shell`, c'est à dire l'interpréteur de commandes `bash`.


 ```console
marie@ordinateur:~$ mkdir playist
marie@ordinateur:~$ cd playlist
marie@ordinateur:~/playlist$ echo "Gestion de morceaux de musique" > README.md
marie@ordinateur:~/playlist$ git init
marie@ordinateur:~/playlist$ git add .
marie@ordinateur:~/playlist$ git commit -m "première importation"
```

Voici les explications :
 * Marie crée le dossier `playist`, et avec la commande `cd`, elle change
   de dossier pour en faire son dossier de travail.
 * dès la ligne 3, on voit que le dossier courant est `~/playlist` ;
   Marie inscrit une ligne dans le nouveau fichier fichier `README.md`
 * à la ligne 4, Marie demande à Git d'initialiser un dépôt dans le
   dossier courant ; git y créera un dossier caché pour son usage interne.
 * à la ligne 5, Marie déclare qu'on « ajoute », c'est à dire que Git doit
   prendre en considération tout ce qui se trouve là, à partir du dossier
   courant, `.` (un point) dans le jargon de `bash`.
 * à la ligne 6, Marie demande à Git de « commettre », c'est à dire de
   prendre en charge tous les fichiers qui ont été marqués par « l'ajout »
   précédent. La commande `git commit` nécessite un commentaire, c'est
   pourquoi le commentaire, entre guillemets, est donné juste après 
   l'option `-m`. Si on oublie de donner un commentaire, Git en réclamera un
   quand même, quitte à faire une courte session interactive.
   
À retenir, pour la survie :
---------------------------


 commande     | explication
--------------|---------------------------------------------------------
 `git init`   | **Crée l'infrastructure** nécessaire au fonctionnement de Git. On le fait juste une fois par projet. Un dossier caché est créé.
 `git add`    | **Marque un fichier** (ou plusieurs) pour être pris en compte lors du prochain `git commit` ; les fichiers qui n'ont pas été « ajoutés » seront ignorés si on tente de les « commettre ».
 `git commit` | **Git créra un enregistrement** pour chacun des fichiers précedemment ajoutés. Un commentaire est obligatoire ; on peut le marquer sur la ligne de commande, après l'option `-m`.


Je crée un nouveau fichier, et je l'ajoute :
--------------------------------------------

À l'aide d'un éditeur de texte, Marie crée un fichier `p1.csv`, qui
contient ceci:

### Contenu du fichier p1.csv : ###

```
"Id";"Titre";"Artiste";"Année";"Durée";"Genre";
1;"Like a Rolling Stone ";"Bob Dylan";1965;00:06:11;"Folk";"S"
2;"(I Can't Get No) Satisfaction ";"The Rolling Stones";1965;00:03:42;"Rock";"E"
3;"Imagine ";"John Lennon";1971;00:03:07;"Pop";"O"
4;"What's Going On ";"Marvin Gaye";1971;00:03:53;"Soul – R&B";"A"
5;"Respect ";"Aretha Franklin";1967;00:02:25;"Pop";"S"
6;"Good Vibrations ";"The Beach Boys";1966;00:03:57;"Pop";"A"
7;"Johnny B. Goode ";"Chuck Berry";1957;00:02:49;"Pop";"C"
8;"Hey Jude ";"The Beatles";1968;00:03:58;"Rock";"C"
9;"Smells Like Teen Spirit ";"Nirvana";1991;00:05:01;"Rock";"E"
10;"What'd I Say ";"Ray Charles";1959;00:06:27;"Pop";"P"
```


Puis Marie ajoute le nouveau fichier :

```console
marie@ordinateur:~/playlist$ git add p1.csv
```

Je modifie le fichier existant REAME.md :
-----------------------------------------

Toujours avec un éditeur de texte, Marie modifie le fichier `REAME.md`.
Au départ, celui-ci contient une seule ligne :

```
Gestion de morceaux de musique
```


Marie modifie le texte pourqu'il contienne à la fin ceci :

```
Gestion de morceaux de musique
==============================

Il s'agit d'un logiciel qui va gérer des \*playlists\*.
```

J'interroge Git pour faire le point :
-------------------------------------

après enregistrement du fichier `REAME.md` modifié, Marie peut demander
à Git où il en est des nouveautés :

```console
marie@ordinateur:~/playlist$ git status
Sur la branche master
Modifications qui seront validées :
  (utilisez "git reset HEAD <fichier>..." pour désindexer)

	nouveau fichier : p1.csv

Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

	modifié :         README.md

```

On voit ci-dessus, à partir de la ligne 2, que Git fait une différence entre
le fichier `p1.csv` qui a été « ajouté », il apparaît avec la mention
*nouveau fichier*.

À la fin, par contre, on voit que la modification qui a touché le fichier
`README.md` ne serait pas validée lors d'un prochain `git commit`.

Je veux en savoir plus sur les changement :
-------------------------------------------

Marie lance la commande `git diff`, qui permet de connaître les différences
qui existent entre l'état actuel du fichier `README.md` et sa version
précédemment « commise » par Git :

```console
marie@ordinateur:~/playlist$ git diff README.md
diff --git a/README.md b/README.md
index 7d40993..7b915fa 100644
--- a/README.md
+++ b/README.md
@@ -1 +1,5 @@
 Gestion de morceaux de musique
+==============================
+
+
+Il s'agit d'un logiciel qui va gérer des *playlists*.
```

On voit, dans les lignes 9 à 11, que quatre lignes exactement ont été
ajoutées dans le fichier `README.md`, et on sait exactement où.

Je veux tout commettre :
------------------------

Marie lance deux commandes, qui ont pour effet de commettre toutes les
nouveautés :

```console
marie@ordinateur:~/playlist$ git add .
marie@ordinateur:~/playlist$  git commit -m "mise place d'une première playlist"
[master 708a476] mise place d'une première playlist
 2 files changed, 15 insertions(+)
 create mode 100644 p1.csv
```

Coup d'œil à l'historique :
---------------------------

Pour connaître l'historique des actions de Git, la commande est
`git log` :

```console
marie@ordinateur:~/playlist$ git log
commit 708a47673bbb6e0dcc21c5cffd0c9ae8c9a9aa45 (HEAD -> master)
Author: Marie Curie <marie.curie@serveur-de-mail.com>
Date:   Thu Mar 19 18:43:14 2020 +0100

    mise place d'une première playlist

commit 7120144213d3103d0af1b5cd7d5b44d5df86f8f7
Author: Marie Curie <marie.curie@serveur-de-mail.com>
Date:   Thu Mar 19 18:32:09 2020 +0100

    première importation
```

On observe ci-dessus un historique, avec les actions les plus récentes
en premier. Chaque action possède un numéro de code :
`708a47673bbb6e0dcc21c5cffd0c9ae8c9a9aa45`, 
`7120144213d3103d0af1b5cd7d5b44d5df86f8f7` ; ces numéros identifient 
de façon unique chaque élément de l'historique. C'est grâce à ces numéros
qu'on peut retourner fouiller dans les versions passées des fichiers.

Simplifier un numéro de version :
---------------------------------

Les longs numéros à quarante chiffres, c'est facile à oublier, et on n'est
pas à l'abri d'une faute de frappe ! Le mieux est de définir des *tags*
ainsi :

```console
marie@ordinateur:~/playlist$ git tag version-1.0
```

En faisant cela, Marie a créé un *tag* simplement nommé `version-1.0`, qui
est un alias pour le numéro à quarante chiffres 
`708a47673bbb6e0dcc21c5cffd0c9ae8c9a9aa45`, comme elle peut aussitôt le
constater :

```console
marie@ordinateur:~/playlist$ git log
commit 708a47673bbb6e0dcc21c5cffd0c9ae8c9a9aa45 (HEAD -> master, tag: version-1.0)
Author: Marie Curie <marie.curie@serveur-de-mail.com>
Date:   Thu Mar 19 18:43:14 2020 +0100

    mise place d'une première playlist

commit 7120144213d3103d0af1b5cd7d5b44d5df86f8f7
Author: Marie Curie <marie.curie@serveur-de-mail.com>
Date:   Thu Mar 19 18:32:09 2020 +0100

    première importation
```

On voit en effet que le *tag* `version-1.0` est associé à la commission
la plus récente. C'est plus facile à mémoriser que 
`708a47673bbb6e0dcc21c5cffd0c9ae8c9a9aa45`.

Encore une modification, mais malheureusement, une erreur :
-----------------------------------------------------------

Marie modifie le fichier `p1.csv` en y ajoutant trois lignes :

```
"Id";"Titre";"Artiste";"Année";"Durée";"Genre";
1;"Like a Rolling Stone ";"Bob Dylan";1965;00:06:11;"Folk";"S"
2;"(I Can't Get No) Satisfaction ";"The Rolling Stones";1965;00:03:42;"Rock";"E"
3;"Imagine ";"John Lennon";1971;00:03:07;"Pop";"O"
4;"What's Going On ";"Marvin Gaye";1971;00:03:53;"Soul – R&B";"A"
5;"Respect ";"Aretha Franklin";1967;00:02:25;"Pop";"S"
6;"Good Vibrations ";"The Beach Boys";1966;00:03:57;"Pop";"A"
7;"Johnny B. Goode ";"Chuck Berry";1957;00:02:49;"Pop";"C"
8;"Hey Jude ";"The Beatles";1968;00:03:58;"Rock";"C"
9;"Smells Like Teen Spirit ";"Nirvana";1991;00:05:01;"Rock";"E"
10;"What'd I Say ";"Ray Charles";1959;00:06:27;"Pop";"P"
11;"My Generation ";"The Who";1965;00:03:17;"Rock";"A"
12;"A Change Is Gonna Come ";"Sam Cooke;1963;00:03:16;"Soul – R&B";"R"
13;"Yesterday ";"The Beatles";1965;00:02:55;"Rock";"R"
```


Puis elle enregistre le fichier, et commet les changements :

```console
marie@ordinateur:~/playlist$ git add .
marie@ordinateur:~/playlist$ git commit -m "trois morceaux de plus dans la playlist p1"
[master d1b7701] trois morceaux de plus dans la playlist p1
 1 file changed, 3 insertions(+)
```

Malheureusement, quand Marie tente d'ouvrir le fichier `p1.csv` à l'aide
de LibreOffice Calc, elle se rend compte que la ligne numéro 13, (celle qui
commence par le nombre 12), est mal prise en compte, elle contient une erreur.

Dans certains cas, les erreurs sont faciles à trouver et à corriger.

Ne trichez pas, ne lisez pas en dessous de cette ligne .... Essayez de trouver
la faute ci-dessus.


Si vous avez trouvé la faute sans tricher, félicitations ! En effet, il
manque juste un guillemet `"` après `Sam Cooke`. Mais Marie n'a pas envie de
chercher ; après tout, ce ne sont que trois lignes, faciles à saisir à nouveau
sans faute, et puis on peut faire confiance à Git : ne conserve-t-il pas
*toutes* les versions successives d'un fichier ?

Abracadabra, je veux le même fichier avant la faute !
-----------------------------------------------------

Quoi de plus simple ?

```console
marie@ordinateur:~/playlist$ git checkout version-1.0 p1.csv
```

Quelle bonne idée de mettre un *tag* sur les versions stables !

La commande `git checkout` permet de récupérer une version commise du
fichier `p1.csv` ; par défaut ce serait la toute dernièrement commise, mais
comme on a commis ce fichier un peu vite, avant de découvrir la faute, il
faut récupérer la version précédente. Heureusement, la *tag* `version-1.0`
avait été défini auparavant, ce qui évite de devoir relire l'historique
avec `git log` et y trouver le numéro à quarante chiffres.

Dans le pire des cas, il fallait relire l'historique, d'où l'intérêt de 
faire des messages pertinents lors du `git commit`. Et si les messages n'aident
pas, eh bien il reste encore une ressource : on lit le manuel de Git, où
il est expliqué que `git checkout` peut aussi travailler sur les dates : si
on est sûr que le fichier était bon la semaine précédente, on sait le 
retrouver ...

À retenir, pour la survie :
---------------------------


 commande       | explication
----------------|-------------------------------------------------------------
 `git tag`      | **Fait une marque** dans le tas de fichiers que Git est en train de gérer. Grâce à celle-ci, on pourra plus tard extraire les fichiers intéressants qui existaient sous cette marque.
 `git log`      | **Permet de regarder l'historique** de ce que Git a géré ; dans la marine anglaise, un *log* désigne un journal de bord. Si les commentaires des `git commit` sont bien conçus, le journal de brd sera facile à comprendre.
 `git checkout` | **Permet de reprendre un fichier** qu'on avait confié à la gestion de Git dans le passé `git checkout` permet de mentionner une version particulière, identifiée par son numéro à 40 chiffre, ou par un *tag* plus facile à manier.


Git, interagir à distance
=========================

Dans le chapitre précedent, il est juste question de « survie » de la
développeuse ou du développeur. L'erreur est humaine, et c'est bien de
laisser aux ordinateurs le travail de gestion des versions des fichiers ;
c'est un travail de « paperasse » assez désagréable, et ce serait un
comble d'ajouter les « erreurs de paperasse » aux erreurs de développement.

Cependant, là où Git excelle, c'est dans l'interaction à distance entre
développeurs.

Le principe est simple : Git est décentralisé
---------------------------------------------

Du moment que deux dépôt Git partagent la même histoire, on a quantités de
commandes utiles qui permettent à plusieurs développeurs de travailler
sur le même code sans se gêner mutuellement. Si on trois développeurs
A, B, C, (Adèle, Bernard, Carome) eh bien il est possible de faire les
échanges entre A et B, B et C, ou A et C directement, par exemple
récupérer chez Bernard les améliorations que Carole a apportées au fichier
p5.csv, tout en gérant les conflits éventuels.

Mais en général, les développeurs s'abonnent à un service web commun
--------------------------------------------------------------------

Le logiciel libre `gitlab` permet de créer un serveur web qui entretient
plusieurs dépôts Git, et permet à des personnes qui s'y connectent, après
authentification, d'interagir avec ces dépôts.

Nous allons illustrer comment Marie Curie, qui travaille avec son
cousin Stanisław Skłodowski (vous imaginez bien  que les noms sont fictifs !),
partage son espace de développement, grâce à un service `gitlab` hébergé
à https://framagit.org/

Notez bien qu'il y a le choix : plusieurs universités possèdent leur propre
service `gitlab`, le site historique https://gitlab.com/ fonctionne très
bien, et il existe d'autres systèmes *grosso modo* équivalents, tel
https://github.com/, racheté par la société Microsoft pour quelques sept
milliards de dollars en 2018.

Créer un compte sur https://framagit.org/
-----------------------------------------

Sans commentaire : c'est à peu près la même procédure que pour la création de
comptes chez d'autres fournisseurs de service, mais il y a quand même une
nuance importante : https://framagit.org/ est un CHATON 
(voir https://chatons.org/) c'est à dire que l'organisation qui gère ce site
s'engage à respecter une éthique forte : entre autres, pas d'exploitation des
données personnelles des utilisateurs !

Quand le compte est créé, on demande à créer un NOUVEAU PROJET. Il
suffit de choisir un nom, par exemple `playlist` ; on vérifie que le
projet sera public, on valide, et voilà.

Placer son travail dans un nouveau projet, chez Framagit
---------------------------------------------------------

Nous admettrons que le pseudonyme de Marie Curie sera, par la suite
`mariasklodowska`.

À partir de ce moment-là une page d'aide apparaît. On va s'intéresser à
la partie intitulée « Push an existing Git repository » ; à prendre
avec « un grain de sel ».

Il y est écrit ceci :

**Push an existing Git repository**

```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://framagit.org/mariasklodowska/playlist.git
git push -u origin --all
git push -u origin --tags
```

les lignes 1 et 2 sont inutiles ; dans le contexte présent, on est déjà
dans le dépôt qui nous intéresse (c'est le sens de `existing_repo`), et
il n'y a pas encore de délaration de dépôt nommé `origin`, il est inutile
donc de chercher à la renommer.

Par contre, les trois lignes suivantes sont importantes :
```
git remote add origin https://framagit.org/mariasklodowska/playlist.git
git push -u origin --all
git push -u origin --tags
```

Notez bien que la première ligne est propre à Marie Curie. Pour un autre
utilisateur, il faudrait bien sûr changer la chaîne `mariasklodowska`.

Voilà, c'est fait ! Marie a recopié et exécuté les trois commandes ci-dessus ;
si on rafraîchit alors la page du Framagit, on voit, au lieu des explications,
apparaître la page d'accueil du projet `mariasklodowska/playlist`.

Marie recopie bien sûr l'URL de son projet dans ses marque-pages.

On invite d'autres membres dans le projet
-----------------------------------------

Ensuite, c'est à son cousin Stanisław Skłodowski de créer un compte
chez Framagit ... Il choisit le pseudonyme `stansklodowski`.

Dès que Stanisław est enregistré, il le signale à sa cousine, qui recherche
dans la page web à gauche une icône de roue dentée, nommée "Settings" et
clique sur le sous-menu "Members". dans la nouvelle page, elle cherche
le premier champ texte, initulé "GitLab member or Email address", où
elle commence à taper `stans` comme il y a encore plein de propositions
possibles, elle va jusqu'à saisir `stansk`, et là, il n'en reste plus qu'un,
elle peut valider la référence à son cousin.

Dans le champ suivant, elle déroule le menu et choisit `Developer`.

Enfin, elle clique sur le bouton "Import". Dès lors, son cousin est membre
du projet, avec des droits étendus.

Les autres membres du projet récupèrent le travail fondateur
------------------------------------------------------------

Dans la page d'accueil du projet (très certainement une URL
semblable à https://framagit.org/mariasklodowska/playlist) il y a à droite un bouton bleu très visible « Clone » : celui-ci permet de dérouler un menu
où on peut choisir l'url de « Clone with Https » ; il y a moyen, d'un clic,
de mettre dans le presse-papier l'URL qui va bien, 
https://framagit.org/mariasklodowska/playlist.git

C'est maintenant au tour de Stanisław de jouer : il tape la commande
suivante dans un terminal, en utilisant le collage (Paste) pour l'url :

```console
stas@ordinateur:~$ git clone https://framagit.org/mariasklodowska/playlist.git
```

Remarque : dans certains terminaux Linux, le raccourci-clavier
Ctrl-Majuscule-V permet le collage ; le clic du bouton du milieu de
la souris le permet aussi.

Stanisław doit répondre à deux questions : donner son pseudonyme, puis
donner son mot de passe (le mot de passe est tapé « à l'aveugle », rien
n'est visible dans le terminal, même pas des points pendant la sisie
du mot de passe). Aussitôt, un nouveau dépôt Git local est créé, dans un
sous-dossier `playlist/`. Le contenu du déôt distant, copie de celui local
de Marie, est répliqué chez Stanisław. Tout, y compris l'historique complet.

Travail derrière un serveur mandataire
--------------------------------------

Dans les lycées, la communication doit souvent traverser un serveur
mandataire (un proxy). Le réglage de Git est nécessaire, on le fait à
l'aide de la commande 

```
git config --global http.proxy http://proxyUsername:proxyPassword@proxy.server.com:port
```

On essaiera déjà de ne rien mettre à la place de `proxyUsername:proxyPassword@`,
c'est à dire qu'on essaie sans le nom d'utlisateur local et son mot de passe.
Si on a les moyens de surfer sur Internet, il y a de fortes chances que
le serveur mandataire laisse passer la communication, car il dispose de
l'identification de l'utilisateur dans un cache. Autrement dit : d'abord
utiliser un navigateur web, ensuite tenter un `git clone`.

la partie `proxy.server.com` est à remplacer par une adresse IP résolue
localement, et `port` se remplace par le numéro de port du service de proxy.
Dans le cas du lycée Jean Bart de Dunkerque, ça donne : 


```
git config --global http.proxy http://172.16.0.254:3128
```

Tirer et pousser avec Git
=========================

Quand une équipe de développeurs est constituée, la routine est la suivante :

Stanisław se met au travail : il change de dossier (`cd`) pour être dans
le dossier que Git contrôle. Puis, il « tire » les nouveautés :

```console
stas@ordinateur:~/playlist$ git pull
```

... authentification, attente de la réponse... Si Marie ou d'autres
développeurs  de l'équipe ont « poussé » leur travail ver le Gitlab,
Stanisław récupère les nouveautés.

Des conflits peuvent se produire : il faut réconcilier
------------------------------------------------------

Il arrive que Git signale un conflit ; c'est le cas si Marie a modifié
un fichier, et que Stanisław a modifié localement ce même fichier.

Dans ce cas, la démarche est simple : Stanisław ouvre le fichier qui
contient le conflit. Cela se présente sous la forme de quelques lignes,
clairement identifiées : on voit ce qui vient de chez Marie, on voit
ce qui vient de chez Stanisław. Il suffit de « faire sa cuisine », et
ôter toutes les lignes *spéciales* spécifiques à Git, pour ne laisser
que le code utile. Cette démarche se nomme la RÉCONCILIATION.

Après réconciliation, il faut faire un `git commit` assorti d'un message
indiquant en peu de mots en quoi la réconciliation a consisté.

Une séance de travail typique
-----------------------------


Nous disions donc, ... Stanisław se met au travail : il change de dossier
(`cd`) pour être dans le dossier que Git contrôle. Puis, il « tire »
les nouveautés ...

```console
stas@ordinateur:~/playlist$ git pull
```

... avec un peu de chance, rien à réconcilier, donc Stanisław travaille
durant une demi-heure, puis il fait les manœuvres pour que Git se charge de
conserver ses avancées :
 * `git status`, pour voir ce qui est nouveau ;
 * `git add .`, pour signaler tout, ou `git add <nom de fichier>` pour ne
   signaler qu'un fichier, ou plus en utilisant des *jokers*.
 * `git commit -m "un message approprié"` pour commettre tous les fichiers
   précédemment signalés.

Enfin, Stanisław envoie les nouveautés vers le dépôt distant, grâce à :
```console
stas@ordinateur:~/playlist$ git push
```

Voilà tout le cycle de travail.

Juste un bémol : si quelqu'un a été très vite, peut-être que le `git
push` est momentanément refusé car il y a des nouveautés dans le dépôt
distant... Alors, `git pull` pour les reprendre, et `git push` pour
envoyer son propre travail.

À retenir, pour échanger les travaux :
--------------------------------------

 commande     | explication
--------------|---------------------------------------------------------
 `git pull`   | **Récupération des nouveautés** que d'autres ont poussées vers le dépôt commun (ce dépôt commun est défini par l'URL spéciale https://framagit.org/mariasklodowska/playlist.git, et ses pages web sont servies à l'URL « normale »  https://framagit.org/mariasklodowska/playlist).
  `git push`  | **Envoi des nouveaux développements** ; quand on a créé des nouveautés localement, et que ces nouveautés ont été « commises », alors la commande `git push` les répliquera dans le dépôt distant, celui de Framagit. Les autres membres du projet pourront y accéder.
  

