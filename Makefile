
all: GuideSurvieGit.pdf GuideSurvieGit.html fiche.pdf

%.pdf: %.md Makefile
	pandoc --template=latex.template $< --pdf-engine=xelatex --toc --highlight-style=pygments -o $@

%.html: %.md Makefile
	pandoc $< -s --highlight-style=pygments -o $@

clean:
	rm -f *.aux *.log *.toc *~ *.out

.PHONY: all clean



