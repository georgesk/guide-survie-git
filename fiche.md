---
title: "Une fiche-mémento pour Git"
lang: fr
...

Une fiche-mémento pour Git
==========================

commande      | explication
 -------------|---------------------------------------------------------
 `git init`   | **Crée l'infrastructure** nécessaire au fonctionnement de Git. On le fait juste une fois par projet. Un dossier caché est créé.
 `git add`    | **Marque un fichier** (ou plusieurs) pour être pris en compte lors du prochain `git commit` ; les fichiers qui n'ont pas été « ajoutés » seront ignorés si on tente de les « commettre ».
 `git commit` | **Git créra un enregistrement** pour chacun des fichiers précedemment ajoutés. Un commentaire est obligatoire ; on peut le marquer sur la ligne de commande, après l'option `-m`.
 `git tag`      | **Fait une marque** dans le tas de fichiers que Git est en train de gérer. Grâce à celle-ci, on pourra plus tard extraire les fichiers intéressants qui existaient sous cette marque.
 `git log`      | **Permet de regarder l'historique** de ce que Git a géré ; dans la marine anglaise, un *log* désigne un journal de bord. Si les commentaires des `git commit` sont bien conçus, le journal de brd sera facile à comprendre.
 `git checkout` | **Permet de reprendre un fichier** qu'on avait confié à la gestion de Git dans le passé `git checkout` permet de mentionner une version particulière, identifiée par son numéro à 40 chiffre, ou par un *tag* plus facile à manier.
 `git pull`   | **Récupération des nouveautés** que d'autres ont poussées vers le dépôt commun (ce dépôt commun est défini par l'URL spéciale https://framagit.org/mariasklodowska/playlist.git, et ses pages web sont servies à l'URL « normale »  https://framagit.org/mariasklodowska/playlist).
  `git push`  | **Envoi des nouveaux développements** ; quand on a créé des nouveautés localement, et que ces nouveautés ont été « commises », alors la commande `git push` les répliquera dans le dépôt distant, celui de Framagit. Les autres membres du projet pourront y accéder.
